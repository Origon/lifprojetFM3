# Système de sondage/vote anonymisé
Réalisé dans le cadre du LifProjet de l'Université Lyon 1 - Projet FM3

# Pré-requis

## Back-end

* Compilateur prenant en charge le C++14: **GCC** 5+ ou **clang** 3.4+
* Système de build: **meson** ou **qmake**

À l'avenir, un système de base NoSQL clé/valeur pourra être nécessaire. Pour le moment, le système
de fichier est utilisé comme base K/V.

## Front-end

* **Python 3.4** ou supérieur
* Bibliothèques SQLite3

# Installation & mise en route

## Back-end

Voir [backend/readme.md](backend/readme.md)

## Front-end

Voir [web/readme.md](web/readme.md)

