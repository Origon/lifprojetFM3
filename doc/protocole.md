# Protocole FM3

Le protocole FM3 permet la communication entre le processus de serveur crypto et un nombre
arbitraire de processus d'interface (serveur web par ex.).

Il s'agit d'un protocole basé sur des datagrammes circulant sur un socket UNIX:
`socket(AF_UNIX, SOCK_DGRAM, 0);`

**Pourquoi des sockets au lieu d'une API et d'une lib ?**
En utilisant une lib, la mémoire utilisée pour la cryptographie se retrouve dans le même espace
d'adressage que le processus l'utilisant. En cas de faille dans le processus appellant, des données
sensibles pourraient être récupérées.
Utiliser un socket UNIX et un processus à part permet l'isolation des espaces d'adressage par le
noyau, fournissant plus de sécurité.

**Pourquoi des sockets au lieu de tuyaux (*pipes*) ?**
Les sockets sont plus paramétrables, peuvent prendre plusieurs connexions à la fois, et ne sont
pas nécessairement bloquants, contrairement aux *pipes*.

**Pourquoi `SOCK_DGRAM` ?**
Les messages ont besoin d'être reçus en entier pour être traités, mais leur ordre n'importe pas.
De plus, avec un protocole *stream*, il faut ajouter des infos de délimitation, ce qui peut causer
une désynchronisation en cas de bug si il y a plus ou moins de données que ce qui est annoncé.
Avec `SOCK_DGRAM`, le noyau s'assure de la taille.

**Les datagrammes ne sont-ils pas limités en taille ?**
Si. À la fois sur la taille des datagrammes individuels et sur la taille de la file d'attente.
Sur Linux, le paramètre *sysctl* `net.core.wmem_max` détermine le max d'un datagramme, mais il n'est
quasiement jamais réglé en dessous de `65536` (64Kio), ce qui est amplement suffisant.
Quant à la file d'attente, les socktets *stream* sont soumis à autant de limites que les sockets
*datagram*.

## Format d'un datagramme

Les données numériques sur plus d'un octet sont passés *dans l'ordre machine* (*native endian*).

### En-tête

L'en-tête fait 8 octets (pour des raisons d'alignement mémoire).

    46 4D 33 xx cc cc 00 00

`xx` (`uint8`) = version du protocole. Pour l'instant, `00`.  
`cc cc` (`uint16`) = indice de la commande.

### Commandes

#### `00 00`: No-op

**Bidirectionnel**.

**Fonction:** Commande vide, sans instruction. Ne doit pas être envoyé en temps normal.

**Format:** contenu ignoré.

**Réponse:** Aucune.

#### `00 01`: Ping

**Bidirectionnel**.

**Fonction:** Envoie une demande de ping.

**Format:** identifiant du ping sur un nombre arbitraire d'octets dans l'intervalle [4, 512].

**Réponse:** `00 01 Pong` avec le même identifiant.

#### `00 02`: Pong

**Bidirectionnel**.

**Fonction:** Réponse à un ping.

**Format:** identifiant du ping associé.

**Réponse:** Aucune.
