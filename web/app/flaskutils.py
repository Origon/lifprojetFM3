class AppUtils():
    def __init__(self, app):
        self.app = app
        self.globals = {}
        app.template_context_processors[None].append(self.inject_globals)

    def tplglobal(self, glob):
        self.globals[glob.__name__] = glob
        return glob

    def inject_globals(self):
        return self.globals
