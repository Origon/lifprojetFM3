from flask import render_template, request
from flask_babel import gettext
from app import app
from app.models import *
from flask_cas import login_required
from app import cas
from .fm3client import FM3Client,MessageType
from app import yamlpoll
import umsgpack

@app.context_processor
def inject_vars():
    return dict(app=app, cas=cas)

@app.route('/')
@app.route('/index')
def index():
    user = {'username': ''}
    return render_template('index.html', title='Home', user=user)

@app.route('/list')
def poll_list():
    polls = Poll.query.filter_by(public=True).order_by(Poll.date_created.desc())
    return render_template('list.html', title=gettext("Poll list"), polls=polls)

@app.route('/poll/<pollid>')
@login_required
def poll_display(pollid):
    user = User.query.filter_by(username=cas.username).first()
    if user is None:
        user = User(username=cas.username)
        db.session.add(user)
        db.session.commit()

    poll = Poll.query.filter_by(unique_id=pollid.encode('ascii')).first()
    if(poll!=None):
        kwargs = {
            'title': poll['title'],
            'poll': poll,
            'username': user.username
        }
        return render_template('poll.html', **kwargs)
    else:
        return render_template('404.html')
	
@app.route('/test')
def test():
    user = User.query.filter_by(username='ElementW').first()
    if user is None:
        user = User(username='ElementW')
        db.session.add(user)
        db.session.commit()

    poll = Poll.query.filter_by(title='Ceci est un test chargé depuis YAML').first()
    if poll is None:
        poll = yamlpoll.load("""
title: "Ceci est un test chargé depuis YAML"
lang: fre
public: true
elements:
- type: question
  title: "Êtes-vous allé aux JDLL 2018 ?"
  content: "**Si vous avez hésité** mais __avez eu__ un *empèchement de dernière* `minute`, dites Non."
  question_type: Radio
  choices:
  - Oui \o/
  - Non :(
- type: question
  title: "Comptez-vous aller aux JDLL 2019 ?"
  content: "Pour vous convaincre d'y aller, regardez ce chat: ![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/33/Brown_and_white_tabby_cat_with_green_eyes-Hisashi-03.jpg/800px-Brown_and_white_tabby_cat_with_green_eyes-Hisashi-03.jpg)"
  question_type: Radio
  choices:
  - Oui
  - Non
  - Peut-être
  - Seulement si `pyg` est là
- type: question
  title: "Quelle nourriture aimeriez-vous voir à la cafèt' des JDLL ?"
  question_type: Checkbox
  choices:
  - Bière (CoLiBibine rpz)
  - Saucisson
  - Épinards
""")
        poll.owner = user
        db.session.add(poll)
        db.session.commit()
    from io import StringIO
    log = StringIO()
    log.write('{}\n{}\n{}\n'.format(user, poll, poll.elements))
    log.write(str(poll.elements[0].choices))
    return render_template('debuglog.html', logtext=log.getvalue())

@app.route('/send', methods=['POST'])
@login_required
def send_to_backend():
    poll = Poll.query.filter_by(unique_id=request.form['poll_id'].encode('ascii')).first()
    incarray = [[] for x in range(poll.elements.filter_by(element_type='question').count())]
    for key, val in request.form.items(multi=True):
        if key.startswith('q'):
            incarray[int(key[1:])].append(int(val[1:]))
    nranswers = []
    for question in poll.elements.filter_by(element_type='question').order_by(PollQuestion.position.desc()):
        nranswers.append(len(question.choices))
    data = {
        'poll_id': request.form['poll_id'],
        'indices': incarray,
        'nranswers': nranswers
    }
    print(data)
    try:
        client = FM3Client()
        #on passe au serveur le chiffré à "incrémenter"
        client.send(MessageType.Increment, umsgpack.packb(data))
        print(client.recv())
        return render_template('success.html')
    except:
        return render_template('error.html')

@app.errorhandler(404)
@app.errorhandler(Exception)
def page_not_found(error):
	return render_template('404.html')
