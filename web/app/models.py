import enum
import time
from app import app, apputils, db, utils
Column = db.Column
from sqlalchemy.orm import relationship

class Subscriptable:
    def __getitem__(self, item):
        return getattr(self, item)

@apputils.tplglobal
class TextFormat(enum.Enum):
    PlainText = 0
    Markdown = 1

@apputils.tplglobal
class PollElement(db.Model, Subscriptable):
    __tablename__ = 'poll_elements'
    id = Column(db.Integer, primary_key=True)
    poll_id = Column(None, db.ForeignKey('polls.id'), nullable=False)
    poll = relationship('Poll', back_populates='elements')
    element_type = Column(db.String(24), nullable=False)
    position = Column(db.SmallInteger, nullable=False)
    __mapper_args__ = {'polymorphic_on': element_type}

class PollNote(PollElement):
    __tablename__ = 'poll_notes'
    id = Column(None, db.ForeignKey('poll_elements.id'), primary_key=True)
    text_format = Column(db.Enum(TextFormat), nullable=False, default=TextFormat.Markdown)
    text_content = Column(db.UnicodeText(), nullable=False)
    __mapper_args__ = {'polymorphic_identity': u'note'}

@apputils.tplglobal
class QuestionType(enum.Enum):
    Null = 0
    Checkbox = 1
    Radio = 2
    Slider = 3
    FreeText = 104
    Image = 105
    File = 106

@apputils.tplglobal
class CheckboxStyle(enum.Enum):
    Checks = 0
    SwitchButtons = 1

@apputils.tplglobal
class RadioStyle(enum.Enum):
    Radios = 0
    Dropdown = 1

@apputils.tplglobal
class SliderStyle(enum.Enum):
    Slider = 0
    StarRating = 1

class PollQuestion(PollElement):
    __tablename__ = 'poll_questions'
    id = Column(None, db.ForeignKey('poll_elements.id'), primary_key=True)
    title_format = Column(db.Enum(TextFormat), nullable=False, default=TextFormat.Markdown)
    title_content = Column(db.UnicodeText(), nullable=False, default='')
    text_format = Column(db.Enum(TextFormat), nullable=False, default=TextFormat.Markdown)
    text_content = Column(db.UnicodeText(), nullable=False, default='')
    question_type = Column(db.Enum(QuestionType), nullable=False)
    question_type_style = Column(db.Integer, nullable=True)
    choices = relationship('PollQuestionChoice', back_populates='question')
    __mapper_args__ = {'polymorphic_identity': u'question'}

    def __repr__(self):
        return '<Question #{} "{}"/"{}" for {}>'.format(self.id, self.title_content,
                self.text_content, self.poll)

class PollQuestionChoice(db.Model, Subscriptable):
    __tablename__ = 'poll_questions_choices'
    question_id = Column(None, db.ForeignKey('poll_elements.id'), primary_key=True)
    question = relationship('PollQuestion', back_populates='choices')
    index = Column(db.SmallInteger, primary_key=True)
    position = Column(db.SmallInteger, nullable=False)
    text_format = Column(db.Enum(TextFormat), nullable=False, default=TextFormat.Markdown)
    text_content = Column(db.UnicodeText(), nullable=False, default='')

    def __repr__(self):
        return '<Choice {}.{} @{} "{}">'.format(self.question, self.index, self.position,
                self.text_content)

class User(db.Model, Subscriptable):
    __tablename__ = 'users'
    id = Column(db.Integer, primary_key=True)
    username = Column(db.Unicode(256), nullable=False, default='')
    auths = relationship('UserAuth', back_populates='user')
    polls = relationship('Poll', back_populates='owner')

    def __repr__(self):
        return '<User #{} "{}">'.format(self.id, self.username)

class UserAuth(db.Model, Subscriptable):
    __tablename__ = 'user_auths'
    id = Column(db.Integer, primary_key=True)
    user_id = Column(None, db.ForeignKey('users.id'))
    user = relationship('User', back_populates='auths')
    auth_source = Column(db.String(32), nullable=False)
    auth_data = Column(db.LargeBinary)

class Poll(db.Model, Subscriptable):
    __tablename__ = 'polls'
    id = Column(db.Integer, primary_key=True)
    # 43 = longueur de base64(32) moins le '=' de padding
    unique_id = Column(db.String(43), unique=True, default=utils.sqlBase64id)
    owner_id = Column(None, db.ForeignKey('users.id'))
    owner = relationship('User', back_populates='polls')
    lang = Column(db.String(8), nullable=False)
    title = Column(db.Unicode(256), nullable=False, default='')
    date_created = Column(db.BigInteger, nullable=False, default=time.time)
    public = Column(db.Boolean, nullable=False, default=True)
    passwd_hash = Column(db.String(128), nullable=True)
    passwd_salt = Column(db.String(32), nullable=True)
    is_draft = Column(db.Boolean, default=True)
    elements = relationship('PollElement', back_populates='poll', lazy='dynamic')

    def __repr__(self):
        return '<Poll #{} "{}" by {}>'.format(self.id, self.title, self.owner)
