#!/usr/bin/env python3

import socket
import sys
import struct
import os
import logging
import weakref
import time
from enum import IntEnum

class MessageType(IntEnum):
    NoOp = 0x0000
    Ping = 0x0001
    Pong = 0x0002
    Increment = 0x0003


class FM3Client(object):
    Header = struct.Struct('=3sBH2x')
    Magic = b'FM3'
    ProtocolVersion = 0

    def __init__(self):
        weakref.finalize(self, self.close)
        self.logger = logging.getLogger(__name__)

        # Create a UDS socket
        self.sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)

        # Connect the socket to the port where the server is listening
        server_address = '/tmp/fm3sock'
        self.logger.info("Connecting to {}".format(server_address))
        self.sockaddr = '/tmp/fm3client.{}'.format(os.getpid())
        try:
            self.sock.connect(server_address)
            self.sock.bind(self.sockaddr)
        except FileNotFoundError as e:
            raise RuntimeError("Failed to connect to FM3 backend, is the server running?") from e
        except socket.error as msg:
            self.logger.error(msg)
            raise
        self.logger.info("Connected")

    def close(self):
        if self.sock is None:
            return
        self.logger.info("Closing")
        self.sock.close()
        self.sock = None
        try:
            os.unlink(self.sockaddr)
        except FileNotFoundError:
            pass

    def send(self, msgtype, data=None):
        header = self.Header.pack(self.Magic, self.ProtocolVersion, msgtype.value)
        if data is not None:
            message = header + data
        else:
            message = header
        self.sock.send(message)

    def recv(self):
        data = self.sock.recv(64 * 1024)
        header = self.Header.unpack(data[0:self.Header.size])
        if header[0] != self.Magic:
            raise RuntimeError("Bad magic")
        if header[1] != self.ProtocolVersion:
            raise RuntimeError("Bad protocol version")
        return (MessageType(header[2]), data[self.Header.size:])

    def doPing(self):
        payload = os.urandom(128)
        self.send(MessageType.Ping, payload)
        recvstart = time.perf_counter()
        resp = self.recv()
        recvend = time.perf_counter()
        assert resp[0] == MessageType.Pong
        assert resp[1] == payload
        return recvend - recvstart

if __name__ == '__main__':
    client = FM3Client()

    print("sending manual Ping")
    client.send(MessageType.Ping)
    rcv = client.recv()
    print("received {} {}".format(rcv[0].name, rcv[1]))

    print("sending auto Ping")
    print("{} µs latency".format(client.doPing() * 1000000))
