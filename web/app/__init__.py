from flask import Flask
from app.config import Config
from flask_assets import Environment, Bundle
from flask_babel import Babel
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from app.flaskutils import AppUtils
from flask_cas import CAS

app = Flask(__name__)
app.config.from_object(Config)
apputils = AppUtils(app)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
assets = Environment(app)
babel = Babel(app)

cas = CAS(app)
app.config['CAS_SERVER'] = "https://cas.univ-lyon1.fr/"
app.config['CAS_AFTER_LOGIN'] = "index"
app.config['CAS_AFTER_LOGOUT'] = "http://localhost:5000/index" # c'est pas beau, on le sait
app.secret_key="lol" # import os -> os.urandom(24)

js = Bundle(
    'js/a.js',
    filters=None if app.debug else 'jsmin', output='gen/packed.js')
assets.register('js_all', js)

scss = Bundle(
    'css/style.scss',
    'css/questionstyle.scss',
    depends = [
      'css/styleconfig.scss',
    ], filters='pyscss' + ('' if app.debug else ',cssmin'), output='gen/style.css')
assets.register('scss_style', scss)

fonts = Bundle(
    'res/fonts/DejaVuSansMono/stylesheet.css',
    'res/fonts/Neris/stylesheet.css',
    'res/fonts/OpenSans/stylesheet.css',
    filters='cssrewrite' + ('' if app.debug else ',cssmin'), output='gen/fonts.css')
assets.register('fonts', fonts)

from app import models, routes, utils
