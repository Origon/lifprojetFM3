from app.models import *
import yaml

def load(txtdata):
    data = yaml.load(txtdata)
    poll = Poll(
            title=data['title'],
            lang=data['lang'],
            public=data.get('public', False),
            passwd_hash=data.get('passwd_hash', None),
            passwd_salt=data.get('passwd_salt', None))
    eidx = 0
    for elm in data['elements']:
        if elm['type'] == 'question':
            q = PollQuestion(
                    poll=poll,
                    position=eidx,
                    title_content=elm['title'],
                    text_content=elm.get('content', ''),
                    question_type=QuestionType[elm.get('question_type', 'Radio')])
            cidx = 0
            for choice in elm['choices']:
                ch = PollQuestionChoice(
                        question=q,
                        index=cidx,
                        position=cidx,
                        text_content=choice)
                cidx += 1
        eidx += 1
    return poll
