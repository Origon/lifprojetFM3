import mistune
from os import urandom
from base64 import urlsafe_b64encode
from flask import Markup

from app import app

def inlinemd(text):
    return Markup(mistune.markdown(text, escape=True,
                                   parse_inline_html=True, parse_block_html=False)[3:-5])
app.jinja_env.filters.setdefault('inlinemd', inlinemd)

def markdown(text):
    return Markup(mistune.markdown(text)[:-1])
app.jinja_env.filters.setdefault('markdown', markdown)

def base64id(bytes):
    return urlsafe_b64encode(urandom(bytes))

def sqlBase64id():
    return base64id(32)[:-1]
