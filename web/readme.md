# Serveur web Flask FM3

## Mise en route automatique

Le dossier `web` dispose d'un script `server.sh` qui simplifie une partie des tâches de mise en
place et maintenance du front-end web. Ce script effectue quelques vérifications sur l'environnement
dans lequel il est démarré avant d'effectuer ses taches et avertit si quelque chose ne va pas.

### Premier démarrage

**Commande:** `./server.sh`

Créé un environnement virtuel Python, télécharge les dépendances puis démarre le serveur.

### Démarrage du serveur

**Commande:** `./server.sh`

Si l'environnement virtuel existe déjà, démarre le serveur.

### Mise à jour des paquets de l'environnement virtuel

**Commande:** `./server.sh update-venv`

Met à jour les paquets PyPi dont dépend le serveur.
Quand `requirements.txt` est mis à jour, lancez cette commande.

### Reconstruction du shcéma de la base de données

**Commande:** `./server.sh rebuild-db`

Si la base est corrompue ou que les modèles dans le code Python sont modifiés, reconstruisez
la base cette commande.

## Manipulation manuelle

L'utilisation d'un environnement `virtualenv` est conseillé. Certaines commandes ci-dessous
sont données en partant du principe que c'est le cas (par ex. script présent dans le PATH,
où `virtualenv` ajoute `<venv>/bin/` à ce dernier).

### Dépendances PyPi

Installez/mettez à jour les dépendances du serveur via `python3 -m pip install -r requirements.txt`,
en tant que root pour une installation globale, avec `--user` pour une installation locale, ou
directement en cas d'utilisation d'un `virtualenv`.

### Lancement

`python3 ./server.py` ou `FLASK_APP=server.py flask run`

