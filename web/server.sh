#!/bin/sh

PY3NAME=python3
PY3=$(command -v "$PY3NAME")
if [ -z "$PY3" ]; then
  echo "$PY3NAME not found in PATH"
  exit 1
fi

PY3REQVERMAJOR=3
PY3REQVERMINOR=4
PY3VERSTR=$("$PY3" --version | cut -c 8-)
PY3VERMAJOR=$(awk -F '.' '{print $1}' <<<"$PY3VERSTR")
PY3VERMINOR=$(awk -F '.' '{print $2}' <<<"$PY3VERSTR")
if [ "$PY3VERMAJOR" -ne "$PY3REQVERMAJOR" ]; then
  echo "Python version $PY3VERSTR doesn't have a major version of 3"
  exit 1
fi
if [ "$PY3VERMINOR" -lt "$PY3REQVERMINOR" ]; then
  echo "Python >= 3.4 is required, only got $PY3VERSTR"
  exit 1
fi

VENVDIR=venv
VENVCREATED=''
if [ '!' -d "$VENVDIR" ]; then
  python3 -m venv "$VENVDIR"
  VENVCREATED=yes
fi
source venv/bin/activate
if [ "x$VENVCREATED" = "xyes" -o "x$1" = "xupdate-venv" ]; then
  python3 -m pip install -r requirements.txt
  if [ "x$1" = "xupdate-venv" ]; then
    exit $?
  fi
fi

export FLASK_APP=server.py
export FLASK_DEBUG=1
if [ "x$1" = "xrebuild-db" ]; then
  rm app/app.db
  rm -rf migrations/
  flask db init || exit $?
  flask db migrate || exit $?
  flask db upgrade
  exit $?
else
  exec flask run "$@"
fi
