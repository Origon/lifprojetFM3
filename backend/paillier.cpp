#include "paillier.h"


/*Paillier::Paillier(){
  RSA r;
  r.generateKey();
  Paillier(r);
}*/

Paillier::Paillier(RSA rsa) :
    rsa(rsa) {
    pk = rsa.getPk();
    pk_pk = pk * pk;
    sk = rsa.getSk();
}

BigInt Paillier::Chiffrer(const BigInt &msg){
    BigInt random;
    random.Random(2048);

    BigInt c = (BigInt::ExpMod(pk+1,msg,pk_pk) * BigInt::ExpMod(random,pk,pk_pk)) % (pk_pk);

    return c;
}

BigInt Paillier::Ajout(const BigInt &c1, const BigInt &c2) {
    BigInt c = (c1 * c2) % pk_pk;
    return c;
}

BigInt Paillier::Dechiffrer(const BigInt &msg_c) {
    BigInt random = BigInt::ExpMod(msg_c, pk.Invert(sk), pk);
    return ( ( ( BigInt::ExpMod(msg_c,1,pk_pk) * BigInt::ExpMod(random.Invert(pk_pk),pk,pk_pk) )%(pk_pk) )-1 )/pk;
}
