#ifndef PAILLIER
#define PAILLIER

#include "BigInt.h"
#include "rsa.h"

class Paillier{
private:
  RSA rsa;
  BigInt sk;
  BigInt pk;
  BigInt pk_pk;

public:
  //Paillier();
  Paillier(RSA rsa);
  BigInt Chiffrer(const BigInt &msg);
  BigInt Ajout(const BigInt &c1, const BigInt &c2);
  BigInt Dechiffrer(const BigInt &msg_c);
};

#endif
