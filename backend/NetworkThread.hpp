#ifndef FM3_NETWORK_THREAD_HPP
#define FM3_NETWORK_THREAD_HPP

namespace fm3 {

class NetworkThread {
public:
    void main();
};

}

#endif /* FM3_NETWORK_THREAD_HPP */
