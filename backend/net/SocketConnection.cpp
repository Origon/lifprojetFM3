#include "SocketConnection.hpp"

#include <iostream>
#include <sstream>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <utility>

#include "../Endian.hpp"

using namespace std;

namespace fm3 {
namespace net {

SocketConnection::SocketConnection(int fd) : //, struct sockaddr_in addr) :
    m_fd(fd),
    //m_addr(addr),
    m_msg_buffer(nullptr) {
}

SocketConnection::SocketConnection(SocketConnection &&c) {
    operator=(std::move(c));
}

SocketConnection& SocketConnection::operator=(SocketConnection &&c) {
    std::swap(m_fd, c.m_fd);
    // std::swap(m_addr, c.m_addr);
    std::swap(m_msg_buffer, c.m_msg_buffer);
    return *this;
}

/*
Port SocketConnection::port() const {
  return m_addr.sin_port;
}

std::string SocketConnection::address() const {
  std::ostringstream oss;
  const uint8_t *addr = reinterpret_cast<const uint8_t*>(&m_addr.sin_addr.s_addr);
  oss <<
      static_cast<int>(addr[0]) << '.' <<
      static_cast<int>(addr[1]) << '.' <<
      static_cast<int>(addr[2]) << '.' <<
      static_cast<int>(addr[3]);
  return oss.str();
}
*/

bool SocketConnection::read(Address &addr, InMessage &msg) {
    constexpr uint BufferSize = 64 * 1024;
    if (m_msg_buffer == nullptr) {
        m_msg_buffer = reinterpret_cast<uint8_t*>(malloc(BufferSize));
    }
    socklen_t len = sizeof(Address);
    int res = ::recvfrom(m_fd, m_msg_buffer, BufferSize, MSG_DONTWAIT,
                         reinterpret_cast<sockaddr*>(&addr), &len);
    if (res < 0) {
        int err = errno;
        if (err == EAGAIN || err == EWOULDBLOCK || err == EINTR) {
            return false;
        } else {
            throw std::runtime_error("recv(2) returned "s + std::to_string(res) + ": " +
                    strerror(err));
        }
    }
    if (len == 0) {
        return false;
    }
    // Transfer buffer ownership to message
    msg.fromData(m_msg_buffer, res);
    m_msg_buffer = nullptr;
    return true;
}

void SocketConnection::write(const Address &addr, const OutMessage &msg) {
    if (msg.m_length > Message::MaxDataSize) {
        throw std::runtime_error("Cannot send a message longer than " +
            std::to_string(Message::MaxDataSize) + " bytes");
    }
    OutMessage::Header *hdr = reinterpret_cast<OutMessage::Header*>(msg.m_actualData);
    hdr->magic[0] = 'F';
    hdr->magic[1] = 'M';
    hdr->magic[2] = '3';
    hdr->version = 0;
    hdr->type = static_cast<MessageType>(toTe16(static_cast<uint16_t>(msg.getType())));
    hdr->padding = 0;
    int wr = ::sendto(m_fd, msg.m_actualData, Message::HeaderSize + msg.m_length, 0,
                      reinterpret_cast<const sockaddr*>(&addr), sizeof(Address));
    if (wr == -1){
        throw std::runtime_error("send(2) returned -1: "s + strerror(errno));
    }
}

bool SocketConnection::disconnect() {
    shutdown(m_fd, SHUT_RDWR);
    close(m_fd);
    return true;
}

}
}
