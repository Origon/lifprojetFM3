#ifndef FM3_NET_NETWORK_HPP
#define FM3_NET_NETWORK_HPP

#include <cstdint>
#include <exception>
#include <limits>
#include <type_traits>

#include "../io/MemoryStream.hpp"
#include "Types.hpp"

namespace fm3 {
namespace net {

enum class MessageType : uint16_t {
    NoOp = 0x0000,
    Ping = 0x0001,
    Pong = 0x0002,
    Increment = 0x0003
};
const char* MessageTypeName(MessageType);

class Message : public virtual io::MemoryStream {
protected:
    friend class TCPConnection;
    MessageType m_type;

    Message() {}
    Message(MessageType);
    Message(const Message&) = delete;
    Message& operator=(Message&) = delete;
    Message& operator=(const Message&) = delete;

public:
    struct [[gnu::packed]] Header {
      char magic[3];
      uint8_t version;
      MessageType type;
      uint16_t padding;
    };
    static_assert(sizeof(Header) == 8, "L'en-tête ne fait pas 8 octets");
    static constexpr uint HeaderSize = sizeof(Header);
    static constexpr uint MaxTotalSize = std::numeric_limits<uint16_t>::max();
    static constexpr uint MaxDataSize = MaxTotalSize - HeaderSize;

    virtual ~Message() {}

    inline MessageType getType() const { return m_type; }
};

class InMessage : public Message, public io::InMemoryStream {
protected:
    friend class SocketConnection;
    void setType(MessageType type);
    void fromData(const void *data, SizeT);
    void free();

public:
    InMessage();
    ~InMessage();

    inline const void* getCursorPtr(uint advanceCursor = 0) {
      m_cursor += advanceCursor;
      return &(m_data[m_cursor-advanceCursor]);
    }
    inline const void* getCursorPtr() const {
      return &(m_data[m_cursor]);
    }

    std::string dump() const;
};

class OutMessage : public Message, public io::OutMemoryStream {
protected:
    friend class SocketConnection;
    mutable uint8_t *m_actualData;
    void fit(SizeT) override;

public:
    OutMessage(MessageType t = MessageType::NoOp);
    ~OutMessage();

    inline void setType(MessageType t) { m_type = t; }
};

}
}

#endif /* FM3_NET_NETWORK_HPP */
