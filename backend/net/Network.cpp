#include "Network.hpp"

#include <chrono>
#include <cstdlib>
#include <cstring>
#include <sstream>
#include <memory>

// Debug
#include <iomanip>

namespace fm3 {
namespace net {

const char* MessageTypeName(MessageType t) {
    using T = MessageType;
    switch (t) {
    case T::NoOp: return "NoOp";
    case T::Ping: return "Ping";
    case T::Pong: return "Pong";
    case T::Increment: return "Increment";
    }
    return "";
}

Message::Message(MessageType t) :
    MemoryStream(nullptr, 0),
    m_type(t) {
}


InMessage::InMessage() :
    Message(MessageType::NoOp) {
}

InMessage::~InMessage() {
    free();
}

void InMessage::setType(MessageType type) {
    free();
    m_type = type;
    m_length = m_cursor = 0;
    m_data = nullptr;
}

void InMessage::fromData(const void *data, SizeT len) {
    if (len < HeaderSize) {
        throw std::invalid_argument("Message length is smaller than message header");
    }
    const Header *const hdr = static_cast<const Header*>(data);
    if (hdr->magic[0] != 'F' || hdr->magic[1] != 'M' || hdr->magic[2] != '3') {
        throw std::runtime_error("Invalid message magic");
    }
    if (hdr->version != 0) {
        throw std::runtime_error("Unsupported protocol version");
    }
    free();
    m_cursor = 0;
    m_length = len - HeaderSize;
    m_type = hdr->type;
    // m_data/bytes is guaranteed never to be written to, so we can const_cast it
    // Additionally, try and reduce the allocated memory block size
    m_data = reinterpret_cast<uint8_t*>(realloc(const_cast<void*>(data), len)) + HeaderSize;
}

void InMessage::free() {
    if (m_data != nullptr) {
        ::free(m_data - HeaderSize);
    }
    m_type = MessageType::NoOp;
    m_length = m_cursor = 0;
    m_data = nullptr;
}

std::string InMessage::dump() const {
    std::ostringstream oss;
    char buf[3];
    for (SizeT i = 0; i < length(); ++i) {
        sprintf(buf, "%02x", static_cast<int>(m_data[i]));
        oss << buf;
    }
    return oss.str();
}


OutMessage::OutMessage(MessageType t) :
    Message(t),
    m_actualData(nullptr) {
    fit(0);
}

OutMessage::~OutMessage() {
    std::free(m_actualData);
    m_data = nullptr;
}

const static int OutMessage_AllocStep = 1024;
void OutMessage::fit(SizeT len) {
    if (HeaderSize + len <= m_allocated)
        return;
    SizeT targetSize = (((HeaderSize + len) + OutMessage_AllocStep - 1) /
        OutMessage_AllocStep)*OutMessage_AllocStep; // Round up
    using DataT = decltype(m_actualData);
    DataT newActualData = static_cast<DataT>(
        std::realloc(m_actualData, targetSize));
    if (newActualData == nullptr) {
        throw std::bad_alloc();
    }
    m_actualData = newActualData;
    m_data = newActualData + HeaderSize;
    m_allocated = targetSize;
}

}
}
