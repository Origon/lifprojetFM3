#ifndef FM3_NET_SOCKETLISTENER_HPP
#define FM3_NET_SOCKETLISTENER_HPP

#include <cstdint>

#include "SocketConnection.hpp"
#include "Types.hpp"

namespace fm3 {
namespace net {

class SocketListener {
private:
    int m_fd;
    union {
        Port m_port;
        std::string m_unix_path;
    };
    AddressFamily m_family;
    int m_max_pending;

public:
    SocketListener(const std::string &addr);
    SocketListener(const SocketListener&) = delete;
    ~SocketListener();

    int fd() const {
        return m_fd;
    }

    Port port() const {
      return m_port;
    }

    bool listen();
    SocketConnection accept();
    bool disconnect();
};

}
}

#endif /* FM3_NET_SOCKETLISTENER_HPP */
