#include "SocketListener.hpp"

#include <iostream>
#include <stdexcept>
#include <string>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

using namespace std;

namespace fm3 {
namespace net {

SocketListener::SocketListener(const std::string &addr) :
    m_fd(-1),
    // m_port(0),
    m_max_pending(3) {
    m_family = AddressFamily::UNIX;
    new (&m_unix_path) std::string(addr);
}

bool SocketListener::listen() {
    int socket_desc = socket(AF_UNIX, SOCK_DGRAM, 0);
    if (socket_desc == -1) {
        throw std::runtime_error("socket(2) returned " + std::to_string(socket_desc) + ": " +
            strerror(errno));
    }

    struct sockaddr_un server;
    memset(&server, 0, sizeof(server));
    server.sun_family = AF_UNIX;
    if (m_unix_path.size() >= (sizeof(server.sun_path) / sizeof(server.sun_path[0])) - 1) {
        throw std::length_error("UNIX socket path too long");
    }
    strcpy(server.sun_path, m_unix_path.data());
    int bind_ret = ::bind(socket_desc, reinterpret_cast<struct sockaddr*>(&server), sizeof(server));
    if (bind_ret < 0) {
        throw std::runtime_error("bind(2) returned " + std::to_string(bind_ret) + ": " +
            strerror(errno));
    }
    m_fd = socket_desc;
    ::listen(m_fd, m_max_pending);
    return true;
}

SocketConnection SocketListener::accept() {
    struct sockaddr_un client;
    int c = sizeof(client);
    int client_fd = ::accept(m_fd, reinterpret_cast<struct sockaddr*>(&client),
        reinterpret_cast<socklen_t*>(&c));
    if (client_fd < 0) {
        throw std::runtime_error("accept(2) returned " + std::to_string(client_fd) + ": " +
            strerror(errno));
    }
    return SocketConnection(client_fd); //, client);
}

SocketListener::~SocketListener() {
    disconnect();
    if (m_family == AddressFamily::UNIX) {
        m_unix_path.~basic_string();
    }
}

bool SocketListener::disconnect() {
    shutdown(m_fd, SHUT_RDWR);
    ::close(m_fd);
    if (m_family == AddressFamily::UNIX) {
        unlink(m_unix_path.c_str());
    }
    return true;
}

}
}
