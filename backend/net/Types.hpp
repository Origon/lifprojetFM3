#ifndef FM3_NET_TYPES_HPP
#define FM3_NET_TYPES_HPP

#include <cstdint>
#include <stdexcept>

namespace fm3 {
namespace net {

class Exception : public std::exception {
};

using Port = uint16_t;

enum class AddressFamily {
    UNIX,
    INet4,
    Inet6
};

}
}

#endif /* FM3_NET_TYPES_HPP */

