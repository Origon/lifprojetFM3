#ifndef FM3_NET_SOCKETCONNECTION_HPP
#define FM3_NET_SOCKETCONNECTION_HPP

// #include <arpa/inet.h>
#include <string>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#include "Network.hpp"
#include "Types.hpp"

namespace fm3 {
namespace net {

class SocketConnection {
private:
    int m_fd;
    // struct sockaddr_in m_addr;

    uint8_t *m_msg_buffer;

public:
    using Address = ::sockaddr_un;

    SocketConnection(int fd); //, struct sockaddr_in addr);
    SocketConnection(const SocketConnection&) = delete;
    SocketConnection(SocketConnection&&);

    SocketConnection& operator=(const SocketConnection&) = delete;
    SocketConnection& operator=(SocketConnection&&);

    int fd() const {
        return m_fd;
    }

    bool connected() const {
        return m_fd >= 0;
    }

    /* Port port() const;
    std::string address() const; */

    bool read(Address&, InMessage&);
    void write(const Address&, const OutMessage&);
    bool disconnect();
};

}
}

#endif /* FM3_NET_SOCKETCONNECTION_HPP */
