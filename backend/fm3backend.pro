TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -g

SOURCES += \
    BigInt.cpp \
    rsa.cpp \
    PrimGen.cpp \
    net/Network.cpp \
    net/SocketConnection.cpp \
    net/SocketListener.cpp \
    io/MemoryStream.cpp \
    io/Stream.cpp \
    NetworkThread.cpp \
    db/DataBase.cpp \
    db/FilesystemDB.cpp \
    paillier.cpp

HEADERS += \
    BigInt.h \
    Endian.hpp \
    rsa.h \
    net/Network.hpp \
    net/SocketConnection.hpp \
    net/SocketListener.hpp \
    net/Types.hpp \
    io/MemoryStream.hpp \
    io/Stream.hpp \
    NetworkThread.hpp \
    io/EPoll.hpp \
    db/DataBase.hpp \
    db/FilesystemDB.hpp \
    paillier.h

unix|win32: LIBS += -lgmp -lpthread
