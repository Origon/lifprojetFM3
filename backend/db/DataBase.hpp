#ifndef FM3_DB_DATA_BASE_HPP
#define FM3_DB_DATA_BASE_HPP

#include <string>

namespace fm3 {
namespace db {

class LockedEntry;

class DataBase {
protected:
    friend class LockedEntry;

    LockedEntry makeLock(const std::string &key);

    virtual void unlock(const std::string &key) = 0;
    virtual void set(const std::string &key, const std::string &value) = 0;
    virtual std::string get(const std::string &key) const = 0;
    virtual void remove(const std::string &key) = 0;

public:
    virtual ~DataBase();

    virtual bool has(const std::string &key) const = 0;
    virtual LockedEntry lock(const std::string &key) = 0;
};

class LockedEntry {
protected:
    friend class DataBase;

    DataBase &m_db;
    const std::string m_key;

    LockedEntry(DataBase &db, const std::string &key);

public:
    ~LockedEntry();

    const std::string& key() const {
        return m_key;
    }

    std::string get() const {
        return m_db.get(m_key);
    }
    void set(const std::string &value) const {
        m_db.set(m_key, value);
    }
    void remove() {
        m_db.remove(m_key);
    }
};

}
}

#endif /* FM3_DB_DATA_BASE_HPP */
