#ifndef FM3_DB_FILESYSTEMDB_HPP
#define FM3_DB_FILESYSTEMDB_HPP

#include "DataBase.hpp"

#include <condition_variable>
#include <map>
#include <mutex>

namespace fm3 {
namespace db {

class FilesystemDB : public DataBase {
protected:
    const std::string m_path;
    struct LockMapEntry {
        volatile bool inUse;
        int waiting;
        std::mutex mutex;
        std::condition_variable condVar;
        LockMapEntry();
    };
    std::map<std::string, LockMapEntry> m_lockMap;
    std::mutex m_lockMapMutex;

    void unlock(const std::string &key) override;
    void set(const std::string &key, const std::string &value) override;
    std::string get(const std::string &key) const override;
    void remove(const std::string &key) override;

public:
    FilesystemDB(const std::string &path);
    ~FilesystemDB();

    bool has(const std::string &key) const override;
    LockedEntry lock(const std::string &key) override;
};

}
}

#endif /* FM3_DB_FILESYSTEMDB_HPP */
