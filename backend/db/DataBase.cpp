#include "DataBase.hpp"


#include <iostream>
namespace fm3 {
namespace db {

LockedEntry DataBase::makeLock(const std::string &key) {
    return LockedEntry(*this, key);
}

DataBase::~DataBase() {
}

LockedEntry::LockedEntry(DataBase &db, const std::string &key) :
    m_db(db), m_key(key) {
}
LockedEntry::~LockedEntry() {
    m_db.unlock(m_key);
}

}
}
