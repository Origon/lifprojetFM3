#include "FilesystemDB.hpp"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <system_error>

namespace fm3 {
namespace db {

FilesystemDB::LockMapEntry::LockMapEntry() :
  inUse(true), waiting(0) {
}

LockedEntry FilesystemDB::lock(const std::string &key) {
    std::unique_lock<std::mutex> lock(m_lockMapMutex);
    auto it = m_lockMap.find(key);
    if (it == m_lockMap.end()) {
        // Le fichier n'est pas verouillé, on le lock
        m_lockMap.emplace(std::piecewise_construct,
                std::forward_as_tuple(key),
                std::forward_as_tuple());
    } else {
        // Le fichier est déjà vérouillé, on attend
        lock.unlock();
        std::unique_lock<std::mutex> waitLock(it->second.mutex);
        ++it->second.waiting;
        while (it->second.inUse) {
            it->second.condVar.wait(waitLock);
        }
        --it->second.waiting;
        it->second.inUse = true;
    }
    return makeLock(key);
}

void FilesystemDB::unlock(const std::string &key) {
    std::lock_guard<std::mutex> lock(m_lockMapMutex);
    auto it = m_lockMap.find(key);
    if (it == m_lockMap.end()) {
        return;
    }
    if (it->second.waiting == 0) {
        // Aucun thread n'attend que le fichier se libère
        m_lockMap.erase(it);
    } else {
        {
            std::lock_guard<std::mutex> lock(it->second.mutex);
            it->second.inUse = false;
        }
        it->second.condVar.notify_one();
    }
}

std::string FilesystemDB::get(const std::string &key) const {
    int fd = open((m_path + '/' + key).c_str(), O_RDONLY | O_NOATIME | O_CLOEXEC);
    if (fd == -1) {
        int error = errno;
        if (error == ENOENT) {
            return "";
        }
        throw std::system_error(error, std::generic_category());
    }
    struct stat fileinfo;
    if (fstat(fd, &fileinfo) != 0) {
        throw std::system_error(errno, std::generic_category());
    }
    std::string value;
    value.resize(fileinfo.st_size);
    ssize_t rd = read(fd, &value[0], fileinfo.st_size);
    if (rd != fileinfo.st_size) {
        throw std::system_error(errno, std::generic_category());
    }
    close(fd);
    return value;
}

void FilesystemDB::set(const std::string &key, const std::string &value) {
    int fd = open((m_path + '/' + key).c_str(), O_WRONLY | O_CREAT | O_TRUNC | O_CLOEXEC, S_IRUSR | S_IWUSR);
    if (fd == -1) {
        throw std::system_error(errno, std::generic_category());
    }
    size_t wr = write(fd, &value[0], value.size());
    if (wr != value.size()) {
        throw std::system_error(errno, std::generic_category());
    }
    close(fd);
}

void FilesystemDB::remove(const std::string &key) {
    int ok = unlink((m_path + '/' + key).c_str());
    if (ok != 0) {
        int error = errno;
        if (error != ENOENT) {
            throw std::system_error(error, std::generic_category());
        }
    }
}

FilesystemDB::FilesystemDB(const std::string &path) :
    m_path(path) {
}

FilesystemDB::~FilesystemDB() {
}

bool FilesystemDB::has(const std::string &key) const {
    int ok = access((m_path + '/' + key).c_str(), R_OK);
    if (ok == 0) {
        return true;
    }
    int error = errno;
    if (error != ENOENT) {
        throw std::system_error(error, std::generic_category());
    }
    return false;
}

}
}
