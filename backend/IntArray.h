#include<vector>
#include<stdint.h>
#include<string.h>
#include <stdio.h>
#include <stdlib.h>

class IntArray {
private:
    using IntType = uint32_t;
    std::vector<IntType> nombres;

public:
    IntType& operator[](size_t idx) {
        return nombres[idx];
    }

    const IntType& operator[](size_t idx) const {
        return nombres[idx];
    }

    void add(IntType val){
        nombres.push_back(val);
    }

    int size(){
        return nombres.size();
    }

    std::vector<IntType> vector(){
        return nombres;
    }

    char* getChar(){
        char c[100];
        for(unsigned int i = 0;i<nombres.size();i++){
            sprintf (c‚ "%d"‚ nombres[i]);
        }
        return c;
    }

};
