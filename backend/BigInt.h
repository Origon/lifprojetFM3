//***************************************************************************************
// BigInt.h :
//				Classe BigInt : gestion de nombres entiers codés sur un nombre élevé de
//								bits.
//---------------------------------------------------------------------------------------
// - Les nombres utilisés sont signés (valeur absolue + bit de signe et non pas en
//	 utilisant le complément à 2).
// - le codage utilise plusieurs entiers non signés de 32 bits pour la sauvegarde des
//	 données.
// - le nombre de bits de codage évolue dynamiquement en fonction de la valeur et des
//	 opérations mathématiques faites dessus.
//***************************************************************************************

#ifndef AFX_BIGINT_H_INCLUDED_
#define AFX_BIGINT_H_INCLUDED_

#include <gmp.h>
#include <math.h>

class BigInt
{
private:
    mpz_t mpz;
    unsigned int size;

public:
    // constructeurs, destructeur
    BigInt() {
        mpz_init(mpz);
        size = 0;
    }
    BigInt(int val) {
        mpz_init_set_si(mpz, val);
        size = 1 + log(val);

    }
    BigInt(const char* szVal, int base = 0) {
        mpz_init_set_str(mpz, szVal, base);
    }
    BigInt(const BigInt& val) {
        mpz_init_set(mpz, val.mpz);
    }
    BigInt(BigInt&& val) {
        mpz[0] = val.mpz[0];
        mpz_init(val.mpz);
    }
    ~BigInt() {
        mpz_clear(mpz);
    }

	// fonctions du générateur aléatoire
    static gmp_randstate_t RandState;
    static void InitRandom();
    inline void Random(int nbBits) {
        InitRandom();
        mpz_urandomb(mpz, RandState, nbBits);
    }
     inline void RandomPrime(int nbBits) {
     Random(nbBits);
     mpz_nextprime(mpz,mpz);
     }

	// exponentielle modulaire
    static BigInt ExpMod(const BigInt& a, const BigInt& b, const BigInt& n) {
        BigInt ret; mpz_powm(ret.mpz, a.mpz, b.mpz, n.mpz); return ret;
    }

	// récupère une représentation affichable du nombre
    char* GetStrValue() const;


    // récupère le nombre de bits
    inline int GetNbBits() const { return mpz_sizeinbase(mpz, 2); }

	// indique si le nombre est pair ou impair
    inline bool IsEven() const { return mpz_even_p(mpz); }
    inline bool IsOdd() const { return mpz_odd_p(mpz); }

    // indique si le nombre est premier ou non
    // 0 si pas premier, 1 si probablement premier, 2 si c'est sûr
    inline int IsPrime() const { return mpz_probab_prime_p(mpz, 40); }

	// affectation en profondeur
    BigInt& operator=(const BigInt& val) {
        mpz_set(mpz, val.mpz);
        return *this;
    }
    BigInt& operator=(BigInt&& val) noexcept {
        mpz_clear(mpz);
        mpz[0] = val.mpz[0];
        mpz_init(val.mpz);
        return *this;
    }

    // Opérateur de conversion permettant d'utiliser les fonctions de GMP
    // directement sur les objets BigInt
    operator mpz_t&() {
        return mpz;
    }
    operator const mpz_t&() const {
        return mpz;
    }

	// division complète (avec quotient et reste)
    inline BigInt DivEuclide(const BigInt& val) {
        BigInt ret; mpz_fdiv_qr(mpz, ret.mpz, mpz, val.mpz); return ret;
    }

    // inversion modulaire
    BigInt Invert(BigInt modulo) const;

	//-----------------------------------------------------------------------------------
	// opérateurs arithmétiques sans modification des opérandes
    inline BigInt operator+(const BigInt& val) const {
        BigInt ret; mpz_add(ret.mpz, mpz, val.mpz); return ret;
    }
    inline BigInt operator-(const BigInt& val) const {
        BigInt ret; mpz_sub(ret.mpz, mpz, val.mpz); return ret;
    }
    inline BigInt operator-() const {
        BigInt ret; mpz_neg(ret.mpz, mpz); return ret;
    }
    inline BigInt operator*(const BigInt& val) const {
        BigInt ret; mpz_mul(ret.mpz, mpz, val.mpz); return ret;
    }
    inline BigInt operator/(const BigInt& val) const {
        BigInt ret; mpz_div(ret.mpz, mpz, val.mpz); return ret;
    }
    inline BigInt operator%(const BigInt& val) const {
        BigInt ret; mpz_mod(ret.mpz, mpz, val.mpz); return ret;
    }

    inline BigInt operator+(int val) const {
        BigInt ret; if (val > 0) mpz_add_ui(ret.mpz, mpz, val); else mpz_sub_ui(ret.mpz, mpz, -val); return ret;
    }
    inline BigInt operator-(int val) const {
        BigInt ret; if (val > 0) mpz_sub_ui(ret.mpz, mpz, val); else mpz_add_ui(ret.mpz, mpz, -val); return ret;
    }
    inline BigInt operator*(int val) const {
        BigInt ret; mpz_mul_si(ret.mpz, mpz, val); return ret;
    }
    inline BigInt operator/(int val) const {
        return operator/(BigInt(val));
        //BigInt ret; mpz_div_si(ret.mpz, mpz, val); return ret;
    }
    inline BigInt operator%(int val) const {
        return operator%(BigInt(val));
        //BigInt ret; mpz_mod_si(ret.mpz, mpz, val); return ret;
    }

    //-----------------------------------------------------------------------------------
    // opérateurs arithmétiques avec modification de l'opérande source
    inline BigInt& operator+=(const BigInt& val) {
        mpz_add(mpz, mpz, val.mpz); return *this;
    }
    inline BigInt& operator-=(const BigInt& val) {
        mpz_sub(mpz, mpz, val.mpz); return *this;
    }
    inline BigInt& operator*=(const BigInt& val) {
        mpz_mul(mpz, mpz, val.mpz); return *this;
    }
    inline BigInt& operator/=(const BigInt& val) {
        mpz_div(mpz, mpz, val.mpz); return *this;
    }
    inline BigInt& operator%=(const BigInt& val) {
        mpz_mod(mpz, mpz, val.mpz); return *this;
    }

    inline BigInt& operator+=(const int val) {
        if (val > 0) mpz_add_ui(mpz, mpz, val); else mpz_sub_ui(mpz, mpz, -val); return *this;
    }
    inline BigInt& operator-=(const int val) {
        if (val > 0) mpz_sub_ui(mpz, mpz, val); else mpz_add_ui(mpz, mpz, -val); return *this;
    }
    inline BigInt& operator*=(const int val) {
        mpz_mul_si(mpz, mpz, val); return *this;
    }
    inline BigInt& operator/=(const int val) {
        operator/=(BigInt(val));
        //mpz_div_si(mpz, mpz, val);
        return *this;
    }
    inline BigInt& operator%=(const int val) {
        operator%=(BigInt(val));
        //mpz_mod_si(mpz, mpz, val);
        return *this;
    }

	//-----------------------------------------------------------------------------------
	// opérateurs d'incrémentation et décrémentation (postfixés et préfixés)
    inline BigInt& operator++() { mpz_add_ui(mpz, mpz, 1); return *this; }
    inline BigInt& operator--() { mpz_sub_ui(mpz, mpz, 1); return *this; }
    inline BigInt operator++(int) {BigInt temp = *this; mpz_add_ui(mpz, mpz, 1); return temp; }
    inline BigInt operator--(int) {BigInt temp = *this; mpz_sub_ui(mpz, mpz, 1); return temp; }

	//-----------------------------------------------------------------------------------
	//opérateurs de comparaison
    inline int Compare(const BigInt& val) const { return mpz_cmp(mpz, val.mpz); }
    inline bool operator==(const BigInt& val) const { return Compare(val) == 0; }
    inline bool operator!=(const BigInt& val) const { return Compare(val) != 0; }
    inline bool operator>(const BigInt& val) const { return Compare(val) > 0; }
    inline bool operator<(const BigInt& val) const { return Compare(val) < 0; }
    inline bool operator>=(const BigInt& val) const { return Compare(val) >= 0; }
    inline bool operator<=(const BigInt& val) const { return Compare(val) <= 0; }

    inline bool operator==(const int val) const { return Compare(BigInt(val)) == 0; }
    inline bool operator!=(const int val) const { return Compare(BigInt(val)) != 0; }
    inline bool operator>(const int val) const { return Compare(BigInt(val)) > 0; }
    inline bool operator<(const int val) const { return Compare(BigInt(val)) < 0; }
    inline bool operator>=(const int val) const { return Compare(BigInt(val)) >= 0; }
    inline bool operator<=(const int val) const { return Compare(BigInt(val)) <= 0; }

	//-----------------------------------------------------------------------------------
	// décalages de bits sans modification de l'opérande source
    inline BigInt operator<<(int dep) {
        BigInt ret; mpz_mul_2exp(ret.mpz, mpz, dep); return ret;
    }
    inline BigInt operator>>(int dep) {
        BigInt ret; mpz_div_2exp(ret.mpz, mpz, dep); return ret;
    }

	//-----------------------------------------------------------------------------------
	// décalages de bits avec modification de l'opérande source
    inline BigInt& operator<<=(int dep) {
        mpz_mul_2exp(mpz, mpz, dep); return *this;
    }
    inline BigInt& operator>>=(int dep) {
        mpz_div_2exp(mpz, mpz, dep); return *this;
    }

	//-----------------------------------------------------------------------------------
	// opérateurs sur les bits sans modification des opérandes
    inline BigInt operator&(const BigInt& val) const {
        BigInt ret; mpz_and(ret.mpz, mpz, val.mpz); return ret;
    }
    inline BigInt operator|(const BigInt& val) const {
        BigInt ret; mpz_ior(ret.mpz, mpz, val.mpz); return ret;
    }
    inline BigInt operator^(const BigInt& val) const {
        BigInt ret; mpz_xor(ret.mpz, mpz, val.mpz); return ret;
    }

    inline BigInt operator&(const int val) const { return operator&(BigInt(val)); }
    inline BigInt operator|(const int val) const { return operator|(BigInt(val)); }
    inline BigInt operator^(const int val) const { return operator^(BigInt(val)); }

	//-----------------------------------------------------------------------------------
	// opérateurs sur les bits avec modification de l'opérande source
    inline BigInt& operator&=(const BigInt& val) {
        mpz_and(mpz, mpz, val.mpz); return *this;
    }
    inline BigInt& operator|=(const BigInt& val) {
        mpz_ior(mpz, mpz, val.mpz); return *this;
    }
    inline BigInt& operator^=(const BigInt& val) {
        mpz_xor(mpz, mpz, val.mpz); return *this;
    }

    inline BigInt& operator&=(const int val) { return operator&=(BigInt(val)); }
    inline BigInt& operator|=(const int val) { return operator|=(BigInt(val)); }
    inline BigInt& operator^=(const int val) { return operator^=(BigInt(val)); }

    int get_tab(unsigned int bloc, unsigned int taille_bloc);
    void add_tab(unsigned int bloc,unsigned int taille_bloc, unsigned int value);
};
//static_assert(sizeof(BigInt) == sizeof(mpz_t), "BigInt a un surcoût en espace");

#endif	// AFX_BIGINT_H_INCLUDED_
