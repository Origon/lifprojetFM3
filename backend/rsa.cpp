#include "rsa.h"

#include <random>
#include <stdexcept>

RSA::RSA() {
}

void RSA::generateKey() {
    const int bits = 2048, ecart = 100;
    std::minstd_rand0 rand((std::random_device()()));
    std::uniform_int_distribution<int> distriBitsPQ(bits, bits + ecart);
    p.RandomPrime(distriBitsPQ(rand));
    q.RandomPrime(distriBitsPQ(rand));
    createED(std::uniform_int_distribution<int>(bits - ecart, bits)(rand));
}

BigInt RSA::euclide(BigInt a, BigInt b) {
    BigInt s = 0;
    BigInt r = 1;
    BigInt q = 0;
    BigInt temp = 0;
    BigInt b_sauv = b;
    BigInt u = 1;

    while(r != 0)
    {
        q = a / b;
        r = a % b;

        temp = s;
        s = u - q * s;
        u = temp;

        a = b;
        b = r;
    }

    BigInt inv = u;
    if(u < 0)
    {
        inv *= -1;
        inv = b_sauv - (inv % b_sauv);
    }
    else
        inv = u % b_sauv;

    return inv;
}

BigInt RSA::getE() const {
    return e;
}

BigInt RSA::getD() const {
    return d;
}

BigInt RSA::getSk() const {
    return sk;
}

BigInt RSA::getPk() const {
    return n;
}

void RSA::createED(int bits) {
    sk = (p - 1) * (q - 1);

    e.RandomPrime(bits);

    BigInt d = euclide(e, sk);

    // vérification du calcul (nécessaire dans certains cas)
    BigInt verif = (d * e) % sk;

    if (verif != 1) {
        throw std::runtime_error("d n'est pas l'inverse modulaire de e");
    }

    this->d = d;
    this->e = e;
    this->n = p * q;
}

BigInt RSA::chiffrer(BigInt msg) {
    return BigInt::ExpMod(msg,e,n);
}

BigInt RSA::dechiffrer(BigInt msg_c) {
    return BigInt::ExpMod(msg_c,d,n);
}
