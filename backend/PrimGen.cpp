#include <chrono>
#include <iostream>
#include <stdio.h>
#include <string>

#include "NetworkThread.hpp"
#include "rsa.h"
#include "paillier.h"

template<class T>
using chrono_cast = std::chrono::duration<double, T>;

using std::chrono::steady_clock;

int main(int argc, char* argv[]) {
    using namespace std::string_literals;
    if (argc >= 2 && "-s"s == argv[1]) {
        fm3::NetworkThread nt;
        nt.main();
        return 0;
    }
    RSA r;
    auto start = steady_clock::now();
    r.generateKey();
    auto end = steady_clock::now();
    auto diff = end - start;
    std::cout << "Génération de la clé: " << chrono_cast<std::milli>(diff).count() << " ms" << std::endl;

    start = steady_clock::now();
    BigInt a = r.chiffrer(BigInt(2024));
    end = steady_clock::now();
    diff = end - start;
    std::cout << "Chiffrement         : " << chrono_cast<std::milli>(diff).count() << " ms" << std::endl;

    start = steady_clock::now();
    char *dechif = r.dechiffrer(a).GetStrValue();
    end = steady_clock::now();
    diff = end - start;
    std::cout << "Déchiffrement       : " << chrono_cast<std::milli>(diff).count() << " ms" << std::endl;
    printf("Le message était : %s\n", dechif);

    Paillier p(r);
    BigInt c1 = p.Chiffrer(40);
    BigInt c2 = p.Chiffrer(2);
    BigInt c = p.Ajout(c1,c2);
    printf("%s\n", p.Dechiffrer(c).GetStrValue());

}
