#include "Stream.hpp"

#include <stdexcept>

#include "../Endian.hpp"

namespace fm3 {
namespace io {

void OutStream::writeString(const std::string &str) {
  if (str.size() > UINT16_MAX)
    throw std::length_error("String too long");
  uint16_t len = static_cast<uint16_t>(str.size());
  writeU16(len);
  writeData(str.c_str(), len);
}
std::string InStream::readString() {
  uint16_t len = readU16();
  char *data = new char[len];
  readData(data, len);
  std::string str(data, len);
  delete[] data;
  return str;
}

void OutStream::writeBool(bool v) {
  writeU8(v);
}
bool InStream::readBool() {
  return readU8();
}

void OutStream::writeString32(const std::u32string &str) {
  if (str.size() > UINT16_MAX)
    throw std::length_error("String too long");
  uint16_t len = static_cast<uint16_t>(str.size());
  writeU16(len);
  writeData(str.c_str(), len * sizeof(char32_t));
}
std::u32string InStream::readString32() {
  uint16_t len = readU16();
  char32_t *data = new char32_t[len];
  readData(data, len*sizeof(char32_t));
  std::u32string str(data, len);
  delete[] data;
  return str;
}

void OutStream::writeI64(int64_t i) {
  const uint64_t in = toTe64(reinterpret_cast<const uint64_t&>(i));
  writeData(&in, 8);
}
int64_t InStream::readI64() {
  int64_t val;
  readData(&val, 8);
  const uint64_t valh = fromTe64(val);
  return reinterpret_cast<const int64_t&>(valh);
}

void OutStream::writeU64(uint64_t i) {
  const uint64_t in = toTe64(i);
  writeData(&in, 8);
}
uint64_t InStream::readU64() {
  uint64_t val;
  readData(&val, 8);
  return fromTe64(val);
}

void OutStream::writeI32(int32_t i) {
  const uint32_t in = toTe32(reinterpret_cast<const uint32_t&>(i));
  writeData(&in, 4);
}
int32_t InStream::readI32() {
  int32_t val;
  readData(&val, 4);
  const uint32_t valh = fromTe32(val);
  return reinterpret_cast<const int32_t&>(valh);
}

void OutStream::writeU32(uint32_t i) {
  const uint32_t in = toTe32(i);
  writeData(&in, 4);
}
uint32_t InStream::readU32() {
  uint32_t val;
  readData(&val, 4);
  return fromTe32(val);
}

void OutStream::writeI16(int16_t i) {
  const uint16_t in = toTe16(reinterpret_cast<const uint16_t&>(i));
  writeData(&in, 2);
}
int16_t InStream::readI16() {
  int16_t val;
  readData(&val, 2);
  const uint16_t valh = fromTe16(val);
  return reinterpret_cast<const int16_t&>(valh);
}

void OutStream::writeU16(uint16_t i) {
  const uint16_t in = toTe16(i);
  writeData(&in, 2);
}
uint16_t InStream::readU16() {
  uint16_t val;
  readData(&val, 2);
  return fromTe16(val);
}

void OutStream::writeI8(int8_t i) {
  writeData(&i, 1);
}
int8_t InStream::readI8() {
  int8_t val;
  readData(&val, 1);
  return val;
}

void OutStream::writeU8(uint8_t i) {
  writeData(&i, 1);
}
uint8_t InStream::readU8() {
  uint8_t val;
  readData(&val, 1);
  return val;
}

void OutStream::writeFloat(float f) {
  union { float f; uint32_t u; } cvt = { f };
  const uint32_t un = toTe32(cvt.u);
  writeData(&un, sizeof(float));
}
float InStream::readFloat() {
  uint32_t un;
  readData(&un, sizeof(float));
  union { uint32_t u; float f; } cvt = { fromTe32(un) };
  return cvt.f;
}

void OutStream::writeDouble(double d) {
  union { double d; uint64_t u; } cvt = { d };
  const uint64_t un = toTe64(cvt.u);
  writeData(&un, sizeof(double));
}
double InStream::readDouble() {
  uint64_t un;
  readData(&un, sizeof(double));
  union { uint64_t u; double d; } cvt = { fromTe64(un) };
  return cvt.d;
}

}
}
