#ifndef FM3_IO_EPOLL_HPP
#define FM3_IO_EPOLL_HPP

#include <sys/epoll.h>
#include <unistd.h>

namespace fm3 {
namespace io {

class EPoll {
public:
  int fd;

  enum class EventType : uint32_t {
    In = EPOLLIN,
    Out = EPOLLOUT
  };
  struct Event {
    EventType type;
    int fd;
  };

  EPoll() {
    fd = ::epoll_create1(0);
  }
  ~EPoll() {
    close(fd);
  }

  EPoll(const EPoll&) = delete;
  EPoll& operator=(const EPoll&) = delete;

  EPoll(EPoll&&) = delete;
  EPoll& operator=(EPoll&&) = delete;

  void addEvent(EventType evt, int fd) {
    epoll_event epevt;
    epevt.events = static_cast<uint32_t>(evt);
    epevt.data.fd = fd;
    ::epoll_ctl(this->fd, EPOLL_CTL_ADD, fd, &epevt);
  }

  int wait(Event &evt, int timeout = -1) {
    epoll_event epevt;
    int ret = ::epoll_wait(fd, &epevt, 1, timeout);
    if (ret <= 0) {
      return ret;
    }
    evt.type = static_cast<EventType>(epevt.events);
    evt.fd = epevt.data.fd;
    return ret;
  }
};

}
}

#endif /* FM3_IO_EPOLL_HPP */
