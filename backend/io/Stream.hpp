#ifndef FM3_IO_IO_STREAM_HPP
#define FM3_IO_IO_STREAM_HPP

#include <cstdint>
#include <string>

namespace fm3 {
namespace io {

class Stream {
public:
  using SizeT = uint64_t;
  using PosT = SizeT;
  using OffT = int64_t;
};

class SeekableStream : public virtual Stream {
public:
  enum Whence {
    Set,
    Current,
    // End
  };

  virtual PosT tell() const = 0;
  virtual void seek(OffT, Whence = Set) = 0;
  inline void seek(PosT pos) {
    seek(static_cast<OffT>(pos), Whence::Set);
  }

  inline void rewind() {
    seek(static_cast<OffT>(0));
  }
};

class SizedStream : public virtual SeekableStream {
public:
  virtual SizeT length() const = 0;
  virtual SizeT remaining() const {
    return length() - tell();
  }
};

class InStream : public virtual Stream {
public:
  virtual std::string readString();
  virtual std::u32string readString32();
  virtual bool readBool();
  virtual int64_t readI64();
  virtual uint64_t readU64();
  virtual int32_t readI32();
  virtual uint32_t readU32();
  virtual int16_t readI16();
  virtual uint16_t readU16();
  virtual int8_t readI8();
  virtual uint8_t readU8();
  virtual float readFloat();
  virtual double readDouble();
  virtual void readData(void *data, SizeT len) = 0;
};

class OutStream : public virtual Stream {
public:
  virtual void writeString(const std::string &str);
  virtual void writeString32(const std::u32string &str);
  virtual void writeBool(bool v);
  virtual void writeI64(int64_t i);
  virtual void writeU64(uint64_t i);
  virtual void writeI32(int32_t i);
  virtual void writeU32(uint32_t i);
  virtual void writeI16(int16_t i);
  virtual void writeU16(uint16_t i);
  virtual void writeI8(int8_t i);
  virtual void writeU8(uint8_t i);
  virtual void writeFloat(float f);
  virtual void writeDouble(double d);
  virtual void writeData(const void *data, SizeT len) = 0;
};

}
}

#endif /* FM3_IO_IO_STREAM_HPP */
