//***************************************************************************************
// BigInt.h :
//				Classe BigInt : gestion de nombres entiers codés sur un nombre élevé de
//								bits.
//---------------------------------------------------------------------------------------
// - Les nombres utilisés sont signés (valeur absolue + bit de signe et non pas en
//	 utilisant le complément à 2).
// - le codage utilise plusieurs entiers non signés de 32 bits pour la sauvegarde des
//	 données.
// - le nombre de bits de codage évolue dynamiquement en fonction de la valeur et des
//	 opérations mathématiques faites dessus.
//***************************************************************************************

#include "BigInt.h"

#include <memory>
#include <system_error>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <math.h>
#include<iostream>

decltype(BigInt::RandState) BigInt::RandState;

//***************************************************************************************
// InitRandom : initialisation du générateur aléatoire.
//***************************************************************************************
void BigInt::InitRandom() {
    gmp_randinit_default(RandState);
    BigInt randomMpz;
    {
        constexpr size_t RandomBytesCount = 4096;
        std::unique_ptr<uint8_t[]> randomBytes(new uint8_t[RandomBytesCount]);
        int fd = open("/dev/urandom", O_RDONLY | O_SYNC);
        if (fd == -1) {
            throw std::system_error(errno, std::generic_category());
        }
        if (read(fd, randomBytes.get(), RandomBytesCount) != RandomBytesCount) {
            throw std::system_error(errno, std::generic_category());
        }
        close(fd);
        mpz_import(randomMpz, RandomBytesCount, 1, 1, 0, 0, randomBytes.get());
    }
    gmp_randseed(RandState, randomMpz);
}


//***************************************************************************************
// GetStrValue : récupère une représentation en héxa du nombre.
//***************************************************************************************
char* BigInt::GetStrValue() const { //a modifier (std::string)
    static char szVal[2048];
    mpz_get_str(szVal, 10, mpz);
    return szVal;
}

//***************************************************************************************
//***************************************************************************************

//***************************************************************************************
// Inversion Modulaire : permet d'inverser un nombre et de lui appliquer le modulo.
//***************************************************************************************

BigInt BigInt::Invert(BigInt modulo) const {
  BigInt result;
  mpz_invert(result, mpz, modulo);
  return result;
}
//***************************************************************************************
//***************************************************************************************
//***************************************************************************************
// Getter du Tableau BigInt : permet de renvoyer le résultat du bloc selectionné.
//***************************************************************************************

int BigInt::get_tab(unsigned int bloc, unsigned int taille_bloc){
    if(taille_bloc*bloc<=size){
        char szVal[2048];
        mpz_get_str(szVal, 10, mpz);
        char res[taille_bloc];
        for(unsigned int i=0;i<taille_bloc;i++){
            if((int)(size-taille_bloc*bloc-i-1)<0){
                res[taille_bloc-i-1] = '0';
            }else{
                res[taille_bloc-i-1]= szVal[size-bloc*taille_bloc-i-1];
            }
        }
        return atoi(res);
    }
    return -1;
}
//***************************************************************************************
//***************************************************************************************
//***************************************************************************************
// Ajout d'une valeur dans Tableau BigInt : permet d'entrer une valeur dans le bloc selectionné.
//***************************************************************************************
void BigInt::add_tab(unsigned int bloc, unsigned int taille_bloc, unsigned int value){
    if(taille_bloc*bloc<=2048 && value<pow(10,taille_bloc)){
        mpz_t integ,dix,val;
        mpz_init_set_si(dix, 10);
        mpz_init_set_si(integ, 1);
        mpz_init_set_si(val, value);
        if(size<log10(value) + 1+ bloc*taille_bloc){
            size = 1 + log10(value) + bloc*taille_bloc;
        }

        if(bloc != 0)
            mpz_pow_ui(integ,dix,bloc*taille_bloc);
        mpz_mul(integ,integ,val);
        mpz_add(mpz, mpz, integ);
        mpz_clear(integ);
        mpz_clear(dix);
        mpz_clear(val);
    }
}
//***************************************************************************************
//***************************************************************************************


































