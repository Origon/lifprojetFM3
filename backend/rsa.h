#ifndef RSA_H
#define RSA_H
#include "BigInt.h"

// taille du générateur aléatoire (en bits) si possible multiple de 32
#define RANDOM_SIZE		2048

// nombre d'itérations pour les testes probabilistes
#define	NB_ITER			10

class RSA
{
private:
    BigInt p;
    BigInt q;
    BigInt e;
    BigInt d;
    BigInt n;
    BigInt sk;

    static BigInt euclide (BigInt a, BigInt b);
    void createED(int bits); //permet de d'initialiser E (pour déchiffrer) et D (pour chiffrer)

public:
    RSA();

    void generateKey(); //génére la clé
    BigInt getD() const; //Sers à chiffrer avec q et p de genenereKey
    BigInt getE() const; //Sers à déchiffrer avec q et p de genenereKey

    /**
     * @brief Récupère la clé secrete ("Secret Key")
     * @return Clé secrete
     */
    BigInt getSk() const;

    /**
     * @brief Récupère la clé publique ("Public Key")
     * @return Clé publique
     */
    BigInt getPk() const;

    BigInt chiffrer(BigInt msg); //chiffre le msg
    BigInt dechiffrer(BigInt msg_c); //déchiffre le msg chiffré
};

#endif // RSA_H
