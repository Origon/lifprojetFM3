#include "NetworkThread.hpp"

#include <iostream>
#include <string>
#include <string.h>  // strerror
#include <unistd.h> // unlink

#include "io/EPoll.hpp"
#include "net/Network.hpp"
#include "net/SocketListener.hpp"
#include "subprojects/meiose/include/meiose/meiose.hpp"
#include "subprojects/meiose/include/meiose/debug.hpp"

#include "rsa.h"
#include "paillier.h"

namespace fm3 {

using namespace io;
using namespace net;

void NetworkThread::main() {
    /* les clés utilisées */
    RSA r;
    r.generateKey();
    Paillier p(r);

    using namespace std;

    const char *const fm3sockPath = "/tmp/fm3sock";
    unlink(fm3sockPath);
    SocketListener sl(fm3sockPath);
    sl.listen();
    SocketConnection sc(sl.fd());
    EPoll epoll;
    epoll.addEvent(EPoll::EventType::In, sl.fd());
    cout << "FM3backend v0.1\n";
    cout << "Listening on UNIX socket " << fm3sockPath << endl;
    while (true) {
        EPoll::Event evt;
        int res = epoll.wait(evt);
        if (res == -1) {
            int err = errno;
            if (err != EINTR) {
                throw std::runtime_error(std::string("epoll_wait(2) returned -1: ") + strerror(err));
            }
            continue;
        }
        if (evt.fd == sl.fd()) {
            SocketConnection::Address addr;
            InMessage imsg;
            if (sc.read(addr, imsg)) {
                cout << "Message type " << MessageTypeName(imsg.getType()) << " from " << addr.sun_path << endl;
                switch (imsg.getType()) {
                case MessageType::Ping: {
                    OutMessage omsg(MessageType::Pong);
                    omsg.writeData(imsg.getCursorPtr(), imsg.length());
                    sc.write(addr, omsg);
                } break;
                case MessageType::NoOp: {
                    cout << imsg.dump() <<  endl;
                    OutMessage omsg(MessageType::NoOp);
                    omsg.writeData(imsg.getCursorPtr(), imsg.length());
                    sc.write(addr, omsg);
                } break;
                case MessageType::Increment: {
					std::string str(reinterpret_cast<const char*>(imsg.getCursorPtr()), imsg.length());
					auto var = meiose::msgpack::read(str);
                    cout << meiose::debug::dump(var) <<  endl;
                    OutMessage omsg(MessageType::NoOp);
                    omsg.writeData(imsg.getCursorPtr(), imsg.length());
                    sc.write(addr, omsg);
                } break;
                default:
                    break;
                }
            }
        }
    }
    sl.disconnect();
    cout << "Closed" << endl;
}

}
